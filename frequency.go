package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const SwissGridAPI = "https://www.swissgrid.ch/content/swissgrid/fr/home/operation/grid-data/current-data.apicache.json?path=/content/swissgrid/fr/home/operation/grid-data/current-data/jcr:content/parsys/livedatawidget_10292"

var (
	LastFreqPlan = 50.0
	GridTz       *time.Location
)

func init() {
	var err error
	GridTz, err = time.LoadLocation("Europe/Zurich")
	if err != nil {
		panic(err)
	}
}

type SwissGridItem struct {
	Id           string `json:"id"`
	Label        string `json:"label"`
	Value        string `json:"value"`
	MergeColumns bool   `json:"mergeColumns"`
	IsLabelBold  bool   `json:"isLabelBold"`
	IsValueBold  bool   `json:"isValueBold"`
}
type SwissGridMaker struct {
	Id              string  `json:"id"`
	X               float64 `json:"x"`
	Y               float64 `json:"y"`
	Text1           string  `json:"text1"`
	Text2           string  `json:"text2"`
	TextColor       string  `json:"textColor"`
	BackgroundColor string  `json:"backgroundColor"`
	BorderColor     string  `json:"borderColor"`
	Type            string  `json:"type"`
	Direction       string  `json:"direction"`
	Rotation        *string `json:"rotation"`
	Opacity         float64 `json:"opacity"`
	FontSizeDesktop string  `json:"fontSizeDesktop"`
	FontSizeTablet  string  `json:"fontSizeTablet"`
	FontSizePhone   string  `json:"fontSizePhone"`
}

type SwissGridData struct {
	Data struct {
		Table  []SwissGridItem `json:"table"`
		Marker []SwissGridItem `json:"marker"`
	} `json:"data"`
}

func FetchFrequency() (*SwissGridData, error) {
	res, err := http.Get(SwissGridAPI)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode > 299 {
		return nil, fmt.Errorf("Response failed with status code: %d", res.StatusCode)
	}

	dec := json.NewDecoder(res.Body)
	var data SwissGridData
	err = dec.Decode(&data)
	if err != nil {
		return nil, err
	}

	return &data, err
}

func WriteFrequency(writer TICWriter, freq *SwissGridData) error {
	points := map[string]Point{}
	horodate := time.Now()

	for _, i := range freq.Data.Table {
		if i.Id == "FreqPlan" {
			f, err := strconv.ParseFloat(strings.Fields(i.Value)[0], 64)
			if err != nil {
				return err
			}

			if LastFreqPlan != f {
				points["FREQPLAN"] = Point{
					Data: []byte(fmt.Sprintf("%.0f", f*1000)),
				}
				LastFreqPlan = f
			}
		} else if i.Id == "FreqAct" {
			f, err := strconv.ParseFloat(strings.Fields(i.Value)[0], 64)
			if err != nil {
				return err
			}

			points["FREQ"] = Point{
				Data: []byte(fmt.Sprintf("%.0f", f*1000)),
			}
		} else if i.Id == "GridTimeDev" {
			f, err := strconv.ParseFloat(strings.Fields(i.Value)[0], 64)
			if err != nil {
				return err
			}

			points["GRIDTIMEDEV"] = Point{
				Data: []byte(fmt.Sprintf("%.0f", f*1000)),
			}
		} else if i.Id == "SampleTimestamp" {
			var err error
			horodate, err = time.ParseInLocation("02.01.2006 15:04:05", i.Value, GridTz)
			if err != nil {
				return err
			}
		}
	}

	err := writer.AddPoints(points, horodate)
	if err != nil {
		log.Println("Unable to write frequency point:", err)
	}

	return nil
}
