package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

func parseEnvironmentVariables() (err error) {
	for _, line := range os.Environ() {
		if strings.HasPrefix(line, "LINKY2INFLUX_") {
			err := parseConfigLine(line)
			if err != nil {
				return fmt.Errorf("error in environment (%q): %w", line, err)
			}
		}
	}
	return
}

func parseConfigLine(line string) (err error) {
	fields := strings.SplitN(line, "=", 2)
	orig_key := strings.TrimSpace(fields[0])
	value := strings.TrimSpace(fields[1])

	if len(value) == 0 {
		return
	}

	key := strings.TrimPrefix(orig_key, "LINKY2INFLUX_")
	key = strings.Replace(key, "_", "-", -1)
	key = strings.ToLower(key)

	err = flag.Set(key, value)

	return
}
