package main

var (
	Legacy2Std = map[string]string{
		"BASE":    "EAST",
		"HCHC":    "EASF01",
		"HCHP":    "EASF02",
		"EJPHN":   "EASF01",
		"EJPHPM":  "EASF02",
		"BBRHCJB": "EASF01",
		"BBRHPJB": "EASF02",
		"BBRHCJW": "EASF03",
		"BBRHPJW": "EASF04",
		"BBRHCJR": "EASF05",
		"BBRHPJR": "EASF06",
		"IINST":   "IRMS1",
		"IINST1":  "IRMS1",
		"IINST2":  "IRMS2",
		"IINST3":  "IRMS3",
		"PAPP":    "SINSTS",
	}

	MeasurementGroups = map[string][]string{
		"EAS":    []string{"EAST", "EASF01", "EASF02", "EASF03", "EASF04", "EASF05", "EASF06", "EASF07", "EASF08", "EASF09", "EASF10", "EASD01", "EASD02", "EASD03", "EASD04"},
		"ERQ":    []string{"ERQ1", "ERQ2", "ERQ3", "ERQ4"},
		"IRMS":   []string{"IRMS1", "IRMS2", "IRMS3"},
		"URMS":   []string{"URMS1", "URMS2", "URMS3"},
		"SINSTS": []string{"SINSTS", "SINSTS1", "SINSTS2", "SINSTS3"},
	}

	MeasurementStrings = []string{
		"VTIC",
		"DATE",
		"NGTF",
		"LTARF",
		"STGE",
		"DPM1",
		"FPM1",
		"DPM2",
		"FPM2",
		"DPM3",
		"FPM3",
		"MSG1",
		"MSG2",
		"RELAIS",
		"NTARF",
		"NJOURF",
		"NJOURF+1",
		"PJOURF+1",
		"PPOINTE",
	}

	MeasurementUnits = map[string]string{
		"FREQ":        "mHz",
		"FREQPLAN":    "mHz",
		"GRIDTIMEDEV": "ms",

		"EAST":   "Wh",
		"EASF01": "Wh",
		"EASF02": "Wh",
		"EASF03": "Wh",
		"EASF04": "Wh",
		"EASF05": "Wh",
		"EASF06": "Wh",
		"EASF07": "Wh",
		"EASF08": "Wh",
		"EASF09": "Wh",
		"EASF10": "Wh",
		"EASD01": "Wh",
		"EASD02": "Wh",
		"EASD03": "Wh",
		"EASD04": "Wh",

		"EAIT": "Wh",

		"ERQ1": "VArh",
		"ERQ2": "VArh",
		"ERQ3": "VArh",
		"ERQ4": "VArh",

		"IRMS1": "A",
		"IRMS2": "A",
		"IRMS3": "A",

		"URMS1": "V",
		"URMS2": "V",
		"URMS3": "V",

		"PREF":  "kVA",
		"PCOUP": "kVA",

		"SINSTS":  "VA",
		"SINSTS1": "VA",
		"SINSTS2": "VA",
		"SINSTS3": "VA",

		"SMAXSN":    "VA",
		"SMAXSN1":   "VA",
		"SMAXSN2":   "VA",
		"SMAXSN3":   "VA",
		"SMAXSN-1":  "VA",
		"SMAXSN1-1": "VA",
		"SMAXSN2-1": "VA",
		"SMAXSN3-1": "VA",

		"SINSTI": "VA",

		"SMAXIN":   "VA",
		"SMAXIN-1": "VA",

		"CCASN":   "W",
		"CCASN-1": "W",
		"CCAIN":   "W",
		"CCAIN-1": "W",

		"UMOY1": "V",
		"UMOY2": "V",
		"UMOY3": "V",
	}
)
