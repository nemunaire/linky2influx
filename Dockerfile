FROM golang:alpine as gobuild

RUN apk add --no-cache build-base

WORKDIR /go/src/git.nemunai.re/linky2influx

COPY *.go go.mod go.sum ./

RUN go get -d -v && \
    go build -v -buildvcs=false -ldflags="-s -w"


FROM alpine

RUN apk add --no-cache tzdata

CMD ["linky2influx"]

COPY --from=gobuild /go/src/git.nemunai.re/linky2influx/linky2influx /usr/bin/linky2influx
